import numpy as np
import pandas as pd
import datetime
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from yellowbrick.cluster import KElbowVisualizer
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt, numpy as np
from mpl_toolkits.mplot3d import Axes3D  # este no es necesario en realidad
from sklearn.cluster import AgglomerativeClustering
from scipy.cluster.hierarchy import dendrogram, linkage
from apyori import apriori
from sklearn.model_selection import train_test_split
from matplotlib.colors import ListedColormap
from sklearn import metrics
import warnings
import sys
from sklearn.metrics import adjusted_rand_score, v_measure_score, confusion_matrix

if not sys.warnoptions:
    warnings.simplefilter("ignore")

df = pd.read_csv("set_Sebastian_Bustamante_clean.csv")
X = df
y = df["booking_status"]

X_sample, _, _, _ = train_test_split(
    df, df.index, test_size=0.7, stratify=df.iloc[:, 17], random_state=0
)

Z = linkage(X_sample, method="ward")

# Crear el dendrograma
plt.figure(figsize=(10, 7))
dendrogram(Z)
plt.title("Dendrograma del clustering jerárquico aglomerativo")
plt.xlabel("Índice de la muestra")
plt.ylabel("Distancia")
plt.show()
